import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'

///////////////////////////////////

//Routes
const Home = () => import('./routes/Home.vue');
const Search = () => import('./routes/Search.vue');



///////////////////////////////////

//User Authentication Routes
const UserLogin = () => import('./routes/UserLogin.vue');
const UserSignup = () => import('./routes/UserSignup.vue');
const UserForgot = () => import('./routes/UserForgot.vue');
const UserReset = () => import('./routes/UserReset.vue');
const UserAccounts = () => import('./routes/UserAccounts.vue');


///////////////////////////////////

//Events
const Events = () => import('./routes/Events.vue');
const EventsSubSection = () => import('./routes/EventsSubSection.vue');

//Groups
const Groups = () => import('./routes/Groups.vue');

///////////////////////////////////

import AttendanceBarChart from './routes/AttendanceBarChart.vue'

///////////////////////////////////

//Use the router
Vue.use(Router)

///////////////////////////////////

// //Create an event bus so we can reuse the search bar in the toolbar for different routes
// var $globalSearch = new Vue();
// Vue.prototype.$globalSearch = $globalSearch;

///////////////////////////////////



///////////////////////////////////


var array = [];

///////////////////////////////////

array.push({
    name: 'home',
    path: '/',
    redirect: { name: 'events' }
    // meta: {
    //     title: 'Welcome',
    // },
    // component: Home,
    // props: (route) => ({
    //     photo: route.query.photo,
    // })
})

array.push({
    name: 'events',
    path: '/events',
    meta: {
        title: 'Events Overview',
        requireUser: true,
    },
    component: Events,
    children: [{
        name: 'events.section',
        path: 'overview/:metric',
        meta: {
            title: 'Events Overview',
        },
        component: EventsSubSection,
        props: (route) => ({
            metricName: route.params.metric,
        }),
    }]
})



array.push({
    name: 'groups',
    path: '/groups',
    meta: {
        title: 'Groups Overview',
        requireUser: true,
    },
    component: Events,
    // children: [{
    //     name: 'events.section',
    //     path: 'overview/:metric',
    //     meta: {
    //         title: 'Groups Overview',
    //     },
    //     component: EventsSubSection,
    //     props: (route) => ({
    //         metricName: route.params.metric,
    //     }),
    // }]
})

//////////////////////////////////////

array.push({
    name: 'user.login',
    path: '/user/login',
    meta: {
        title: 'Login',
        denyUser: true,
        search: {
            disabled: true,
        }
    },
    component: UserLogin,
})

array.push({
    name: 'user.signup',
    path: '/user/signup',
    meta: {
        title: 'Signup',
        denyUser: true,
        search: {
            disabled: true,
        }
    },
    component: UserSignup,
})

array.push({
    name: 'user.forgot',
    path: '/user/forgot',
    meta: {
        title: 'Forgot Password',
        denyUser: true,
        search: {
            disabled: true,
        }
    },
    component: UserForgot,
})

array.push({
    name: 'user.reset',
    path: '/user/reset',
    meta: {
        title: 'Reset Your Password',
        denyUser: true,
        disableHeader: true,
        disableFooter: true,
        search: {
            disabled: true,
        }
    },
    component: UserReset,
    props: (route) => ({
        token: route.query.token,
    })
})

array.push({
    name: 'user.accounts',
    path: '/user/accounts',
    meta: {
        title: 'My Accounts',
    },
    component: UserAccounts,
    meta: {
        requireUser: true,
        search: {
            disabled: true,
        }
    },
})

//////////////////////////////////////


array.push({
    name: 'search',
    path: '/search',
    meta: {
        title: 'Search',
    },
    component: Search,
    props: (route) => ({
        keywords: route.query.keywords || '',
        photo: route.query.photo,

    })
})

///////////////////////////////////
array.push({
    name: 'sample.barchart',
    path: '/sample/barchart',
    meta: {
        title: 'SAMPLE: Bar Chart',
    },
    component: AttendanceBarChart,
})

///////////////////////////////////

var router = new Router({
    mode: 'history',
    routes: array,
    scrollBehavior(to, from, savedPosition) {

        //Keep track of where the user was scrolled
        //if they hit the back button
        var pos = 0;
        // Scroll to the top
        if (savedPosition) {
            pos = savedPosition.y;
        }
        document.body.scrollTop = document.documentElement.scrollTop = pos;
    },
});

///////////////////////////////////

router.beforeEach((to, from, next) => {
    //Close the drawer whenever we change route
    store.commit('ui/drawer', false)

    if (to.meta) {
        //Get the user session from fluro
        var user = store.getters['fluro/user'];

        //If the route doesn't allow logged in users
        if (to.meta.requireUser) {
            if (user) {
                return next();
            }

            console.log('Route is only accessible to logged in users')
            return next('/user/login')
        }

        //If the route only allows logged in users
        if (to.meta.denyUser) {
            if (!user) {
                return next();
            }

            console.log('Route is not accessible to logged in users')
            return next('/')
        }
    }


    return next();
})





///////////////////////////////////

export default router;

///////////////////////////////////