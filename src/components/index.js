

//Import Components
import Vue from 'vue';
import StatBox from './reporting/StatBox.vue';
import TrendChart from './reporting/TrendChart.vue';
import PieChart from './reporting/PieChart.vue';
import GaugeChart from './reporting/GaugeChart.vue';
import ChangeIndicator from './reporting/ChangeIndicator.vue';

/////////////////////////////////////////////////////

export default {
    install: function(Vue, options) {

        //Add Fluro Components Globally
        Vue.component('change-indicator', ChangeIndicator);
        Vue.component('statbox', StatBox);
        Vue.component('trend-chart', TrendChart);
        Vue.component('pie-chart', PieChart);
        Vue.component('gauge-chart', GaugeChart);
    }

}


////////////////////////////////////////////////////////////////////
